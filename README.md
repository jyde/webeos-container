# webeos-container

This project consists of the server images (with all the required packages installed) for WebEOS on top of:
- CERN Centos 7 base image
- AlmaLinux 9 base image

## okd4-install automatic update

Based on a [schedule](https://gitlab.cern.ch/webservices/webeos/webeos-container/-/pipeline_schedules), the new release of the images is built and pushed to registery and a MR is opened against [okd4-install](https://gitlab.cern.ch/paas-tools/okd4-install) repo with the latest image tag.
Below is a list of the environment variables that the update job requires:

- `BRANCH_NAME`: name of new branch created in the update process in okd4-install repository
- `COMMIT_USER`: email of user who commits the updation of tags in okd4-install
- `MR_ASSIGNEES`: MR Reviewers on okd4-install
- `OKD4_INSTALL_REPO_PATH`: namespace and repository name of the okd4-install repository
- `OKD4_INSTALL_JOB_NAME`: name of job in the new MR pipeline to run after successfully opening the MR

Details of a new or existing Project Access token of [okd4-install](https://gitlab.cern.ch/paas-tools/okd4-install) repo is to be provided for the update job to be able to access it.

- `UPDATE_BOT_USERNAME`: name of project access token
- `UPDATE_BOT_AUTH_TOKEN`: project access token

### Renewal steps of Project Access Token

The `Project Access Token` is valid for 365 days. Once it expires, we need to create a new one by following the steps below:

1. Go to https://gitlab.cern.ch/paas-tools/okd4-install/-/settings/access_tokens
2. Delete the current `webeos-container-update` token
3. Add a new token with:
    - `webeos-container-update` as token name
    - expiration date to 1 year
    - `Developer` role
    - `api` and `write_repository` scopes
4. After creating the new token, update the value of [UPDATE_BOT_AUTH_TOKEN](https://gitlab.cern.ch/webservices/webeos/webeos-container/-/settings/ci_cd) GitLab CI variable in this repository.


Note that we have added `openshift-admins@cern.ch` in [Pipeline status emails](https://gitlab.cern.ch/webservices/webeos/webeos-container/-/settings/integrations/pipelines_email/edit) to be notified for broken pipelines of `master` branch.
