import argparse
import gitlab
import time
import os

#######################################
#      Expected command line args     #
# 1. Gitlab repo project id           #
# 2. MR id                            # 
# 3. Name of the job                  #
#######################################

AUTH_TOKEN = os.environ["UPDATE_BOT_AUTH_TOKEN"]
HOST = os.environ["CI_SERVER_HOST"]

parser = argparse.ArgumentParser(description='Process args.')
parser.add_argument('--repo', required=True, help='Please provide repository path')
parser.add_argument('--mr_id', required=True, help='Please provide MR id')
parser.add_argument('--job_name', required=True, help='Please provide repo path', nargs="+")
args = parser.parse_args()

project_path = args.repo
mr_id = args.mr_id
job_name = ' '.join(args.job_name)

gl = gitlab.Gitlab(url=f'https://{HOST}', private_token=AUTH_TOKEN)

repo_project_id = gl.projects.get(project_path).id

project = gl.projects.get(repo_project_id, lazy=True)
mr = project.mergerequests.get(mr_id)
latest_pipeline_id = mr.pipelines.list()[0].id
print(f'Retrieving pipeline - {latest_pipeline_id} on MR {mr_id} and Project {repo_project_id}')

latest_pipeline = project.pipelines.get(latest_pipeline_id)
jobs = latest_pipeline.jobs.list()
assert len(jobs) > 0, f'No jobs found in pipeline {latest_pipeline_id}'

integration_test_job = list(filter(lambda job: (job.name == job_name ), jobs))

assert len(integration_test_job) > 0, f'No jobs with the name - {job_name}'
assert len(integration_test_job) == 1, f'Multiple jobs with the name {job_name}'

job_id = integration_test_job[0].id
print(f'Running job - {job_id}')

# Job methods (play, cancel, and so on) are not available on ProjectPipelineJob objects. To use these methods create a ProjectJob object:
job_ProjectJob = project.jobs.get(job_id, lazy=True)
# run only the webeos integration test suite
post_data = {
    "job_variables_attributes": [
        {
            "key": "TEST_SUITES",
            "value": "webeos"
        }
    ]
}

for i in range(0,5):
    try:
        job_ProjectJob.play(post_data=post_data)
    except gitlab.GitlabJobPlayError:
        print("Failed to run job - GitlabJobPlayError exception")
        print("Retrying in 5 seconds")
        time.sleep(5)
        continue
    break
